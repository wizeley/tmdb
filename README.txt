TMDB module

- automatically creates a custom content type named "Movie"

- defines a custom form with  the fields:

  +  Title
  +  Overview
  +  Date

- adds a menu for the form
- adds an admin/config menu (TMDB settings) in Configuration,
   where you can add your own API key. See here how to create your API key:
   https://www.themoviedb.org/assets/static_cache/41b35724525a13c05bb1d63fe7af7621/images/api-create-2.png

- Prepares 3 user permissions: view, submit and administer. (not used at he moment)

The title is searched on the movie database website through the API.

If is found, a new movie node is added with the same title as the forms title
and the body with the content of the form's overview field
and that's it.

If the title is not found, an error message is displayed which informes the user
 that the movie with the title filled on the form does not exist on tmdb website.


tmdb.zip - download from http://access.wizeley.com/tmdb.zip
The module is not finished and maybe won't be. Is just trying to display some coding in Drupal7.















Encountered issues:
Uninstalling the module leaves the content type movie with no delete button
You need to db_update the custom and locked fields in node_type table with hook_uninstall()