<?php
/**
 * @file
 * Aministration form for the tmdb module
 * to setup tmdb API key
 */
/**
 * @return mixed
 */
function tmdb_admin_settings_form() {
  $form = array();
  $form['tmdb_apikey'] = array(
    '#title' => t('TMDB API KEY'),
    '#description' => t('The api key needed to use TMDB API'),
    '#type' => 'textfield',
    '#default_value' => variable_get('tmdb_apikey'),
  );
  return system_settings_form($form);
}
